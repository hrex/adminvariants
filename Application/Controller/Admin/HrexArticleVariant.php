<?php
/**
 * Created by ragaller eCommerce
 * User: hermann
 * Date: 07.11.2020
 * Web: http://www.hrex.de
 * © 2020 Hermann Ragaller
 */

namespace hrex\adminvariants\Application\Controller\Admin;


class HrexArticleVariant extends HrexArticleVariant_parent
{

    /**
     * Saves article variant.
     *
     * 2020-11-07 Kopie der Originalfunktion mit Erweiterungen:
     * - handling der Checkboxen bm_amazon und bm_pdv
     *
     * @param string $sOXID   Object ID
     * @param array  $aParams Parameters
     *
     * @return null
     */
    public function savevariant($sOXID = null, $aParams = null)
    {
        if (!isset($sOXID) && !isset($aParams)) {
            $sOXID = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter("voxid");
            $aParams = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter("editval");
        }

        // varianthandling
        $soxparentId = $this->getEditObjectId();
        if (isset($soxparentId) && $soxparentId && $soxparentId != "-1") {
            $aParams['oxarticles__oxparentid'] = $soxparentId;
        } else {
            unset($aParams['oxarticles__oxparentid']);
        }
        /** @var \OxidEsales\Eshop\Application\Model\Article $oArticle */
        $oArticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);

        if ($sOXID != "-1") {
            $oArticle->loadInLang($this->_iEditLang, $sOXID);
        }

        // checkbox handling
        if (is_array($aParams) && !isset($aParams['oxarticles__oxactive'])) {
            $aParams['oxarticles__oxactive'] = 0;
        }

        // checkbox handling bm_amazon
        if (is_array($aParams) && !isset($aParams['oxarticles__bm_amazon'])) {
            $aParams['oxarticles__bm_amazon'] = 0;
        }

        // checkbox handling bm_pdv
        if (is_array($aParams) && !isset($aParams['oxarticles__bm_pdv'])) {
            $aParams['oxarticles__bm_pdv'] = 0;
        }

        if (!$this->_isAnythingChanged($oArticle, $aParams)) {
            return;
        }

        $oArticle->setLanguage(0);
        $oArticle->assign($aParams);
        $oArticle->setLanguage($this->_iEditLang);

        // #0004473
        $oArticle->resetRemindStatus();

        if ($sOXID == "-1") {
            if ($oParent = $this->_getProductParent($oArticle->oxarticles__oxparentid->value)) {
                // assign field from parent for new variant
                // #4406
                $oArticle->oxarticles__oxisconfigurable = new \OxidEsales\Eshop\Core\Field($oParent->oxarticles__oxisconfigurable->value);
                $oArticle->oxarticles__oxremindactive = new \OxidEsales\Eshop\Core\Field($oParent->oxarticles__oxremindactive->value);
            }
        }

        $oArticle->save();
    }

}