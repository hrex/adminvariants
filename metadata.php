<?php
/**
 * Created by oxid-module-skeleton.
 * Module: <MODULE_NAME>
 * Autor: <AUTHOR_NAME> <<AUTHOR_EMAIL>>
 *
 * @see https://github.com/OXIDprojects/oxrun
 */

$sMetadataVersion = '2.1';

$aModule = array(
    'id'          => 'adminvariants',
    'title'       => "HREX Adminvariants",
    'description' => 'Deaktivierten Zustand der Checkboxen bm_amazon und bm_pdv speichern',
    'thumbnail'   => 'hrexlogo.png',
    'version'     => '1.0.0',
    'author'      => 'Hermann Ragaller',
    'url'         => 'https://hrex.de',
    'email'       => 'info@hrex.de',

    'extend' => [
        \OxidEsales\Eshop\Application\Controller\Admin\ArticleVariant::class
        => \hrex\adminvariants\Application\Controller\Admin\HrexArticleVariant::class,
    ],
);